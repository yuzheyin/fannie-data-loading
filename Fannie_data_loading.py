import re
import pandas as pd

if __name__ == '__main__':

    file = open('C:/Users/yuzhe.yin/Desktop/Fannie_data/Acquisition_2013Q1.txt', 'r')
    lines = file.readlines()
    n = 0
    f = 1
    df = pd.DataFrame(columns=["LOAN_ID", "ORIG_CHN", "Seller.Name", "ORIG_RT",
      "ORIG_AMT", "ORIG_TRM", "ORIG_DTE"
      , "FRST_DTE", "OLTV", "OCLTV", "NUM_BO", "DTI",
      "CSCORE_B", "FTHB_FLG", "PURPOSE", "PROP_TYP"
      , "NUM_UNIT", "OCC_STAT", "STATE", "ZIP_3",
      "MI_PCT", "Product.Type", "CSCORE_C", "MI_TYPE",
      "RELOCATION_FLG"])
    # df = pd.DataFrame(columns=['A', 'B', 'C',
    #                            'd', 'e','f','g','h','i','j','k','l','m','n','o',
    #                            'p','q','r','s','t','u','v','w','x','y'])
    while n < len(lines):
        record = re.split("\|", lines[n].strip())
        print(record)
        df.loc[n] = record
        n = n + 1
        if n % 10000 == 0:
            name = 'C:/Users/yuzhe.yin/Desktop/Fannie_data/Acquisition_2013Q1_'+ str(f) +'.xlsx'
            f = f + 1
            writer = pd.ExcelWriter(name)
            df.to_excel(writer, "Data")
            writer.save()
            df = pd.DataFrame(columns=["LOAN_ID", "ORIG_CHN", "Seller.Name", "ORIG_RT",
                                       "ORIG_AMT", "ORIG_TRM", "ORIG_DTE"
                , "FRST_DTE", "OLTV", "OCLTV", "NUM_BO", "DTI",
                                       "CSCORE_B", "FTHB_FLG", "PURPOSE", "PROP_TYP"
                , "NUM_UNIT", "OCC_STAT", "STATE", "ZIP_3",
                                       "MI_PCT", "Product.Type", "CSCORE_C", "MI_TYPE",
                                       "RELOCATION_FLG"])
