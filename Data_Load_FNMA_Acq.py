
import mysql.connector
import os


# query = ("SELECT loan_id, OLTV FROM FNMA_Acquisition "
#          "WHERE OLTV = 80 ")
#
#
# cursor.execute(query)
#
# for (loan_id, OLTV) in cursor:
#   print(loan_id, OLTV)

directory = os.fsencode("D:\yuzhe\Fannie_data\Acquisition_All")

for file in os.listdir(directory):
    filename = os.fsdecode(file)
    if filename.endswith(".txt"):
        cnx = mysql.connector.connect(user='VWH_Capital', password='VWH_Capital',
                                      host='vwh-mortgage-db.cjrnjqyzrxay.us-east-2.rds.amazonaws.com',
                                      database='FNMA_database')

        # cursor = cnx.cursor()
        cursor = cnx.cursor(buffered=True, dictionary=True)
        print(filename+" Started")
        qstring = "LOAD DATA LOCAL INFILE 'D:/yuzhe/Fannie_data/Acquisition_All/" + filename + "' "
        query = (qstring +
                 "INTO TABLE FNMA_Acquisition " +
                 "FIELDS TERMINATED BY '|' " +
                 "LINES TERMINATED BY '\n'")

        cursor.execute(query)
        cnx.commit()
        print(filename+" Finished")
        cursor.close()
        cnx.close()

