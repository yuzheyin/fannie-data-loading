import re
import pandas as pd

if __name__ == '__main__':

    file = open('C:/Users/yuzhe.yin/Desktop/2017Q1/Performance_2017Q1.txt', 'r')
    lines = file.readlines()
    n = 0
    f = 1
    # df = pd.DataFrame(columns=['A', 'B', 'C',
    #                            'd', 'e','f','g','h','i','j','k','l','m','n','o',
    #                            'p','q','r','s','t','u','v','w','x','y','z','aa','ab','ac','ad','ae'])
    df = pd.DataFrame(columns=["LOAN_ID", "Monthly.Rpt.Prd", "Servicer.Name",
      "LAST_RT", "LAST_UPB", "Loan.Age", "Months.To.Legal.Mat"
      , "Adj.Month.To.Mat", "Maturity.Date", "MSA",
      "Delq.Status", "MOD_FLAG", "Zero.Bal.Code",
      "ZB_DTE", "LPI_DTE", "FCC_DTE", "DISP_DT",
      "FCC_COST", "PP_COST", "AR_COST", "IE_COST",
      "TAX_COST", "NS_PROCS",
      "CE_PROCS", "RMW_PROCS", "O_PROCS", "NON_INT_UPB",
      "PRIN_FORG_UPB_FHFA", "REPCH_FLAG",
      "PRIN_FORG_UPB_OTH", "TRANSFER_FLG"])

    while n < len(lines):
        record = re.split("\|", lines[n].strip())
        print(record)
        df.loc[n] = record
        n = n + 1
        if n % 100 == 0:
            name = 'C:/Users/yuzhe.yin/Desktop/Fannie_data/Performance'+ str(f) +'.xlsx'
            f = f + 1
            writer = pd.ExcelWriter(name)
            df.to_excel(writer, "Data")
            writer.save()
            df = pd.DataFrame(columns=["LOAN_ID", "Monthly.Rpt.Prd", "Servicer.Name",
                                       "LAST_RT", "LAST_UPB", "Loan.Age", "Months.To.Legal.Mat"
                , "Adj.Month.To.Mat", "Maturity.Date", "MSA",
                                       "Delq.Status", "MOD_FLAG", "Zero.Bal.Code",
                                       "ZB_DTE", "LPI_DTE", "FCC_DTE", "DISP_DT",
                                       "FCC_COST", "PP_COST", "AR_COST", "IE_COST",
                                       "TAX_COST", "NS_PROCS",
                                       "CE_PROCS", "RMW_PROCS", "O_PROCS", "NON_INT_UPB",
                                       "PRIN_FORG_UPB_FHFA", "REPCH_FLAG",
                                       "PRIN_FORG_UPB_OTH", "TRANSFER_FLG"])
