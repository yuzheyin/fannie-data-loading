
import mysql.connector
import os




directory = os.fsencode("D:\yuzhe\Fannie_data\Performance_All\part5")

for file in os.listdir(directory):
    filename = os.fsdecode(file)
    if filename.endswith(".txt"):
        cnx = mysql.connector.connect(user='VWH_Capital', password='VWH_Capital',
                                      host='vwh-mortgage-db.cjrnjqyzrxay.us-east-2.rds.amazonaws.com',
                                      database='FNMA_database')

        # cursor = cnx.cursor()
        cursor = cnx.cursor(buffered=True, dictionary=True)
        print(filename+" Started")
        qstring = "LOAD DATA LOCAL INFILE 'D:/yuzhe/Fannie_data/Performance_All/part5/" + filename + "' "
        query = (qstring +
                 "INTO TABLE FNMA_Performance " +
                 "FIELDS TERMINATED BY '|' " +
                 "LINES TERMINATED BY '\n'")

        cursor.execute(query)
        cnx.commit()
        print(filename+" Finished")
        cursor.close()
        cnx.close()

